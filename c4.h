#ifndef C4_H
#define C4_H
#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <SDL.h>

#define MAX_LINES 44
#define MAX_COLS 158

typedef struct s_sdl_sys {
    SDL_Window   *window;
    SDL_Renderer *renderer;
    SDL_Surface  *surfaces[3];
    SDL_Texture  *textures[3];
} t_sdl_sys;

int		ft_isdigit(int c);
int     ft_atoi(const char *str);
void    ft_bzero(void *s, size_t n);
void    *ft_memset(void *b, int c, size_t len);
void	error_create(char *error);
size_t  ft_strlen(const char *s);
int	ft_strcmp(const char *s1, const char *s2);
int check_buf(char *buf);

int ai_get_chip_position(int lines, int cols, int *plateau);
int get_chip_position(int col, int lines, int cols, int *plateau);
int check_state(int lines, int cols, int *plateau);
int negative_slope_scan(int player, int lines, int cols, int *plateau);
int positive_slope_scan(int player, int lines, int cols, int *plateau);
int vert_scan(int player, int lines, int cols, int *plateau);
int hor_scan(int player, int lines, int cols, int *plateau);
void sdl_loop(int lines, int cols, int * plateau);
void print_plateau(int lines, int cols, int *plateau);
void terminal_loop(int lines, int cols, int * plateau);

void        sdl_print_plateau(t_sdl_sys sys, int cols, int lines, int *plateau);
t_sdl_sys   init_sdl(int cols, int lines);
void        destroy_sdl(t_sdl_sys sys);

#endif
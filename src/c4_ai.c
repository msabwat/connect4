#include "c4.h"

int ai_get_chip_position(int lines, int cols, int *plateau) {
    int tmp[lines * cols];
    int new_pos = 0;
    for (int i = 0; i < cols; i++)
    {
        for (int i = 0; i < lines * cols; i++) {
            tmp[i] = plateau[i];
        }
        new_pos = get_chip_position(i, lines, cols, tmp);
        if (new_pos != -1) {
            tmp[new_pos] = 2;
            int res = check_state(lines, cols, tmp);
            if (res == -1) {
                return new_pos;
            }
        }
    }

    for (int i = 0; i < cols; i++)
    {
        for (int i = 0; i < lines * cols; i++) {
            tmp[i] = plateau[i];
        }
        new_pos = get_chip_position(i, lines, cols, tmp);
        if (new_pos != -1) {
            tmp[new_pos] = 1;
            int res = check_state(lines, cols, tmp);
            if (res == 0) {
                return new_pos;
            }
        }
    }
    while (42) {
        new_pos = get_chip_position(rand() % cols, lines, cols, plateau);
        if (new_pos != -1)
            break;
    }
    return new_pos;
}

#include "c4.h"

int vert_scan(int player, int lines, int cols, int *plateau) {
    int cnt = 0;
    int index = 0;
    for (int i = 0; i < cols; i++) {
        index = i;
        cnt = 0;
        while (index < lines * cols) {
            if (plateau[index] != player) {
                cnt = 0;
            }
            else {
                cnt += 1;
            }
            if (cnt == 4) {
                return 1;
            }
            index += cols;
        }
    }
    return 0;
}

int hor_scan(int player, int lines, int cols, int *plateau) {
    int cnt = 0;
    int index = 0;
    for (int i = 0; i < lines * cols; i += cols) {
        index = i;
        cnt = 0;
        for (int j = index; j < index + cols; j++) {
            if (plateau[j] != player) {
                cnt = 0;
            }
            else {
                cnt += 1;
            }
            if (cnt == 4) {
                return 1;
            }
        }
    }
    return 0;
}

int positive_slope_scan(int player, int lines, int cols, int *plateau) {
    int index = (cols * 3);
    int cur = index;
    int cnt = 0;
    for (int i = index; i < lines * cols; i += cols) {
        cur = i;
        cnt = 0;
        while ((cur < lines * cols) && (cur > 0)) {
            if (plateau[cur] != player) {
                cnt = 0;
            }
            else {
                cnt += 1;
            }
            if (cnt == 4) {
                return 1;
            }
            cur = cur - cols + 1;
        }
    }
    for (int i = (lines * cols) - cols + 1; i < (lines * cols) - 3; i += 1) {
        cur = i;
        cnt = 0;
        while ((cur < lines * cols) && (cur > 0)) {
            if (plateau[cur] != player) {
                cnt = 0;
            }
            else {
                cnt += 1;
            }
            if (cnt == 4) {
                return 1;
            }            
            if ((cur + 1) % cols == 0) {
                break;
            }
            cur = cur - cols + 1;
        }
    }
    return 0;
}

int negative_slope_scan(int player, int lines, int cols, int *plateau) {
    int index = 0;
    int cur = index;
    int cnt = 0;
    for (int i = index; i < lines * cols; i += cols) {
        cur = i;
        cnt = 0;
        while ((cur < lines * cols) && (cur >= 0)) {
            if (plateau[cur] != player) {
                cnt = 0;
            }
            else {
                cnt += 1;
            }
            if (cnt == 4) {
                return 1;
            }
            cur += (cols + 1);
        }
    }
    for (int i = 1; i < (cols - 3); i += 1) {
        cur = i;
        cnt = 0;            
        while ((cur < lines * cols) && (cur > 0)) {
            if (plateau[cur] != player) {
                cnt = 0;
            }
            else {
                cnt += 1;
            }
            if (cnt == 4) {
                return 1;
            }            
            if ((cur + 1) % cols == 0) {
                break;
            }
            cur += (cols + 1); 
        }
    }

    return 0;
}

int check_state(int lines, int cols, int *plateau) {
    if ((vert_scan(1, lines, cols, plateau)) || (hor_scan(1, lines, cols, plateau)) 
        || positive_slope_scan(1, lines, cols, plateau) || negative_slope_scan(1, lines, cols, plateau)) {
        return 0;
    }
    if ((vert_scan(2, lines, cols, plateau)) || (hor_scan(2, lines, cols, plateau)) 
        || positive_slope_scan(2, lines, cols, plateau) || negative_slope_scan(2, lines, cols, plateau)) {
        return -1;
    }
    bool cont = false;
    for (int i = 0; i < cols; i ++) {
        if (plateau[i] == 0) {
            cont = true;
            break;
        }
    }
    if (cont == false) {
        return 1;
    }
    return 42;
}

int get_chip_position(int col, int lines, int cols, int *plateau) {
    int start = (lines * cols) - cols + col;
    for (int i = 0; i < lines; i++) {      
        if (plateau[start - (i * cols)] == 0) {
            return start - (i * cols);
        }
    }
    return -1;
}
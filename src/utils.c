#include "c4.h"

static int	toskip(char c)
{
	if ((c <= 32 && c != 27 && c != '\200') && c != '\0')
		return (1);
	return (0);
}

int	ft_isdigit(int c)
{
	return (c >= '0' && c <= '9');
}

int			ft_atoi(const char *str)
{
	int	cnt;
	int	number;
	int	s;

	s = 0;
	number = 0;
	cnt = 0;
	while (toskip(str[cnt]))
		cnt++;
	if (str[cnt] == '+' && str[cnt + 1] >= '0' && str[cnt + 1] <= '9')
		cnt++;
	if (str[cnt] == '-' && str[cnt + 1] >= '0' && str[cnt + 1] <= '9' && s == 0)
	{
		cnt++;
		s = 1;
	}
	while ((str[cnt] >= '0' && str[cnt] <= '9') && str[cnt] != '\0')
	{
		if (s == 0)
			number = 10 * number + str[cnt] - 48;
		else
			number = 10 * number - str[cnt] + 48;
		cnt++;
	}
	return (number);
}

void	ft_bzero(void *s, size_t n)
{
	char	*a;

	if (n != 0)
	{
		a = (char *)s;
		while (n)
		{
			*a = '\0';
			a++;
			n--;
		}
	}
}

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char	*s;

	s = (unsigned char *)b;
	while (len)
	{
		*s = (unsigned char)c;
		s++;
		len--;
	}
	return (b);
}

size_t	ft_strlen(const char *s)
{
	size_t	i;

	i = 0;
	while (*s)
	{
		s++;
		i++;
	}
	return (i);
}

int	ft_strcmp(const char *s1, const char *s2)
{
	while (*s1 && *s1 == *s2)
	{
		s1++;
		s2++;
	}
	return (*(unsigned char *)s1 - *(unsigned char *)s2);
}

int check_buf(char *buf)
{
	if (buf[0] == '\n')
		return -1;
	for (int i = 0;i <= 5 && buf[i] != '\n' && buf[i] != '\0'; i++)
		if (!ft_isdigit(buf[i]) )
			return -1;
	
	return (ft_atoi(buf));
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   interface.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/11 16:00:19 by tlassara          #+#    #+#             */
/*   Updated: 2022/06/12 22:10:47 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "c4.h"

void	error_create(char *error)
{
	write(1, error, ft_strlen(error));
}

/* int who_play(int *tab, int nb_elem)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while(i < nb_elem)
	{
		if (tab[i] == 1)
			j++;
		else if (tab[i] == 2)
			j--;
		i++;
	}
	if (j > 0)
		return (2);
	else 
		return (1);
}
*/
void	sdl_print_plateau(t_sdl_sys sys, int cols, int lines, int *plateau)
{
	//write(1,"raffraichissement image\n", 25 );

	SDL_Rect position;

	for (int i = 0; i < lines * cols; i ++) {
		position.x = (i % lines) * 64;
		position.y = (i / lines) * 64;

		if (plateau[i] == 0)
		{
			SDL_QueryTexture(sys.textures[0], NULL, NULL, &position.w, &position.h);
			SDL_RenderCopy(sys.renderer, sys.textures[0], NULL, &position);
		}
		else if (plateau[i] == 2)
		{
			SDL_QueryTexture(sys.textures[2], NULL, NULL, &position.w, &position.h);
			SDL_RenderCopy(sys.renderer, sys.textures[2], NULL, &position);
		}
		else if (plateau[i] == 1)
		{
			SDL_QueryTexture(sys.textures[1], NULL, NULL, &position.w, &position.h);
			SDL_RenderCopy(sys.renderer, sys.textures[1], NULL, &position);
		}
	}
	SDL_RenderPresent(sys.renderer);				//met la page a jour
}

void destroy_sdl(t_sdl_sys sys) {
	if (sys.surfaces[0])
		SDL_FreeSurface(sys.surfaces[0]);
	if (sys.surfaces[1])
		SDL_FreeSurface(sys.surfaces[1]);
	if (sys.surfaces[2])
		SDL_FreeSurface(sys.surfaces[2]);
	if (sys.renderer)
		SDL_DestroyRenderer(sys.renderer);
	if (sys.textures[0])
		SDL_DestroyTexture(sys.textures[0]);
	if (sys.textures[1])
		SDL_DestroyTexture(sys.textures[1]);
	if (sys.textures[2])
		SDL_DestroyTexture(sys.textures[2]);
	if (sys.window)
		SDL_DestroyWindow(sys.window);
	SDL_Quit();
}

t_sdl_sys init_sdl(int cols, int lines) {
	t_sdl_sys sys = { .renderer = NULL, .surfaces={}, .textures={}};

	if (SDL_Init(SDL_INIT_VIDEO)) {
		error_create("erreur lors de l initialisation de la SDL");
		goto destroy;
	} 	//demarrage SDL avec video / event

	sys.window = SDL_CreateWindow("SDL2", SDL_WINDOWPOS_CENTERED,		 //creation de la fenetre
				SDL_WINDOWPOS_CENTERED,64 * cols, 64 * lines, SDL_WINDOW_SHOWN);
	if (!sys.window) {
		error_create("erreur lors de la creation de la fenetre");
		//goto destroy;
	}

	SDL_SetWindowTitle(sys.window, "connect4");							// titre de la fenetre
	SDL_SetWindowPosition(sys.window, 0, 0);							// ouverture de la fenetre a l emplacement 0,0

	sys.surfaces[1] = SDL_LoadBMP("./assets/imagerouge.BMP");
    sys.surfaces[2] = SDL_LoadBMP("./assets/imagejaune.BMP");
    sys.surfaces[0] = SDL_LoadBMP("./assets/imageVide.BMP");

	if (!sys.surfaces[0] || !sys.surfaces[1] || !sys.surfaces[2]) {
		error_create("erreur lors de la creation d une image");
		//goto destroy;
	}
	
	sys.renderer = SDL_CreateRenderer(sys.window, -1, SDL_RENDERER_ACCELERATED);
	if(!sys.renderer) {
		error_create("erreur lors de la creation du renderer");
		goto destroy;
	}
	
	sys.textures[1] = SDL_CreateTextureFromSurface(sys.renderer,sys.surfaces[1]);
	sys.textures[2] = SDL_CreateTextureFromSurface(sys.renderer,sys.surfaces[2]);
	sys.textures[0] = SDL_CreateTextureFromSurface(sys.renderer,sys.surfaces[0]);
	if (!sys.textures[0] || !sys.textures[1] || !sys.textures[2])
		error_create("erreur lors de la creation d une texture");
	return sys;
destroy:
	destroy_sdl(sys);
	return (t_sdl_sys){}; 
}

void sdl_loop(int lines, int cols, int * plateau) {
	t_sdl_sys sys = init_sdl(cols, lines);
    SDL_Event event;
    sdl_print_plateau(sys, lines, cols, (int *)plateau);
	print_plateau(lines, cols, plateau);
    while (SDL_WaitEvent(&event) >= 0) {
        if (event.type == SDL_QUIT)
            break;
        else if(event.type == SDL_MOUSEBUTTONUP) {
            int col = event.motion.x / 64;
            int pos = get_chip_position(col, lines, cols, (int *)plateau);
            if (pos == -1) {
                write(1,"could not place chip, this column is full!\n", 44);
                continue;
            }
            else {
                plateau[pos] = 1;
            }
            int ret = check_state(lines, cols, (int *)plateau);
            if (ret == 0) { write(1, "success! \n", 11); break; }
            else if (ret == -1) { write(1, "game over! \n", 13); break; }
            else if (ret == 1) { write(1, "draw! \n", 8); break; }
            sdl_print_plateau(sys,lines, cols, (int *)plateau);
			print_plateau(lines, cols, plateau);

            int ai_pos = ai_get_chip_position(lines, cols, (int *)plateau);
            plateau[ai_pos] = 2;
            ret = check_state(lines, cols, (int *)plateau);
            if (ret == 0) { write(1, "success! \n", 11); break; }
            else if (ret == -1) { write(1, "game over! \n", 13); break; }
            else if (ret == 1) { write(1, "draw! \n", 8); break; }
            sdl_print_plateau(sys,lines, cols, (int *)plateau);
			print_plateau(lines, cols, plateau);
        }
    }
    sdl_print_plateau(sys,lines, cols, (int *)plateau);
	print_plateau(lines, cols, plateau);
	write(1,"exiting...\n",11);
	SDL_Delay(1500);
    destroy_sdl(sys);
}

void terminal_loop(int lines, int cols, int * plateau) {
	char buf[5];
	int ret = 0;
	int nbr = 0;

	ft_bzero(buf, 5);
	print_plateau(lines, cols, plateau);
	while (42) {
		printf("\n Please select a column: \n");
		ret = read(0, buf, 4);
		if (ret) {
			if (ft_strlen(buf) > 3) {
				printf("%s is not a valid input!\n", buf);
				ft_bzero(buf, 5);
				continue; 
			}
    	   	nbr = check_buf(buf);
    	    if ((nbr >= 0) && (nbr < cols)) {
				int pos = get_chip_position(nbr, lines, cols, (int *)plateau);
    	        if (pos == -1) {
    	            write(1,"could not place chip, this column is full!\n", 44);
    	            continue;
    	        }
    	        else {
    	            plateau[pos] = 1;
    	        }
    	        ret = check_state(lines, cols, (int *)plateau);
    	        if (ret == 0) { write(1, "success! \n", 11); break; }
    	        else if (ret == -1) { write(1, "game over! \n", 13); break; }
    	        else if (ret == 1) { write(1, "draw! \n", 8); break; }
				print_plateau(lines, cols, plateau);

    	        int ai_pos = ai_get_chip_position(lines, cols, (int *)plateau);
    	        plateau[ai_pos] = 2;
    	        ret = check_state(lines, cols, (int *)plateau);
    	        if (ret == 0) { write(1, "success! \n", 11); break; }
    	        else if (ret == -1) { write(1, "game over! \n", 13); break; }
    	        else if (ret == 1) { write(1, "draw! \n", 8); break; }
				print_plateau(lines, cols, plateau);
			}
			else {
				printf("%s is not a valid input!\n", buf);
				ft_bzero(buf, 5);
				continue;
			}
		}
		ft_bzero(buf, 5);
	}
	print_plateau(lines, cols, plateau);
	write(1,"exiting...\n",11);
}
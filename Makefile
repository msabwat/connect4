NAME=connect4

FLAGS=-Wall -Wextra -Werror

CC=cc

INC=-I.


SRC_NAME= 	c4.c \
			c4_ai.c \
			interface.c \
			utils.c

OBJ_NAME=$(SRC_NAME:.c=.o)

SRC_PATH=src

OBJ_PATH=.obj

SRC=$(addprefix $(SRC_PATH)/,$(SRC_NAME))

OBJS=$(addprefix $(OBJ_PATH)/,$(OBJ_NAME)) 

TEST=

SDL2_INC=-I/usr/include/SDL2

all: makedir $(NAME)

makedir:
	@mkdir -p .obj

test: FLAGS += -g -fsanitize=address,undefined
test: TEST = test
test: makedir $(NAME)

$(NAME): $(OBJ_PATH)/main.o $(OBJS)
	$(CC) $(FLAGS) $(SDL2_INC) $(INC) $(OBJ_PATH)/main.o $(OBJS) -lSDL2 -o $(NAME)

$(OBJ_PATH)/main.o: main.c
	 $(CC) $(FLAGS) $(INC) $(SDL2_INC) -c main.c -o $(OBJ_PATH)/main.o 
$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	$(CC) $(FLAGS) $(INC) $(SDL2_INC) -c $< -o $@

clean:
	rm -fr $(OBJ_PATH)

fclean: clean
	rm -fr $(NAME)
	rm -fr main.o

re: fclean all

#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include "c4.h"

void print_plateau(int lines, int cols, int *plateau) {
    for (int i = 0; i < lines; i++) {
        for (int j = (i * cols); j < (cols * i) + cols; j++) {
            printf(" %d ", plateau[j]);
        }
        printf("\n");
    }
    printf("\n");
}


int main(int ac, char **av) {
    time_t t;
    srand((unsigned) time(&t));
    if ((ac > 4) || (ac < 3)) {
        error_create("error: wrong arguments!\n");
        error_create("usage: ./connect4 <lines> <cols> <--sdl>\n");
        return 1;
    }
    int lines = check_buf(av[1]);
    int cols = check_buf(av[2]);

    if ((lines > MAX_LINES) || (lines < 6)) {
        printf("wrong arguments!\n");
        error_create("usage: ./connect4 <lines> <cols> <--sdl>\n");
        return 1;
    }
    if ((cols > MAX_COLS) || (cols < 7)) {
        printf("wrong arguments!\n");
        error_create("usage: ./connect4 <lines> <cols> <--sdl>\n");
        return 1;
    }
    int plateau[lines * cols];
    ft_memset((int *)plateau, 0, (lines * cols) * 4);
    int r = rand() % 2;
    if (r  == 0) {
        int pos = get_chip_position(rand() % cols, lines, cols, (int *)plateau);
        plateau[pos] = 2;
    }
    if (ac == 4) {
        if (ft_strcmp(av[3], "--sdl") == 0) {
            sdl_loop(lines, cols, (int *)plateau);
        }
        else {
            printf("invalid option!\n");
            error_create("usage: ./connect4 <lines> <cols> <|--sdl>\n");
        }
    }
    else {
        terminal_loop(lines, cols, (int *)plateau);
    }
    return 0;
}
